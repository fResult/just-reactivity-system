(() => { // Removed first line and last line before run `ts-node`

  class Dep {
    constructor(private subscribers: Array<() => void> = []) {
    }

    depend() {
      if (target && !this.subscribers.includes(target)) {
        this.subscribers.push(target)
      }
    }
    notify() {
      this.subscribers.forEach(sub => sub())
    }
  }

  const dep = new Dep()

  let price: number = 5
  let quantity: number = 2
  let total: number = 0
  let target: (() => void) | null = null

  function watcher(callback: () => void) {
    target = callback
    dep.depend()
    target()
    target = null
  }

  watcher(() => total = price * quantity)

  console.log(total)
  price = 20
  console.log(total)
  dep.notify()
  console.log(total)

})() // Removed first line and last line before run `ts-node`
