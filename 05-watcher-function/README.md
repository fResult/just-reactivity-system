# How to test

the first thing to do is get rid of IIFE module (remove first and last line)
```typescript
(() => { /* remove this line */
  // long scripts
})() /* also remove this line */
```
at the root directory, run `ts-node`
```bash
ts-node
```

then call `04-dep-class/index.ts`
```bash
.load 04-dep-class/index.ts
```

test: try to re-assign `price`
```bash
price = 20
```
then enter

test: log `total`
```typescript
console.log('total', total) // total 10
// because reactivity is not notified, so price is still `5`.
```
then enter

test: call `notify()`
```typescript
dep.notify()
```
then enter

test: log `total` again
```typescript
console.log('total', total) // total 40
//because reactivity is notified, so `price` is updated to be 10.
```
then enter

## Problem
When you are already re-assign value or update `data.price`,  
even you can see price is updated by `console.log(data.price)`,  
but `total` does not immediately update, until we call `dep.notify()`,  
then call `console.log(total)` again, coz it was reactived by `data.price`
