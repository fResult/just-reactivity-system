;(() => {
  type Nullable<T> = T | null
  interface IData {
    price: number
    quantity: number
  }

  let total: number = 0
  let target: Nullable<() => void>
  let salePrice: number = 0
  let data: IData = { price: 5, quantity: 2 }

  class Dep {
    constructor(private subscribers: Array<Function> = []) {}

    depend() {
      if (target && !this.subscribers.includes(target)) {
        this.subscribers.push(target)
      }
    }

    notify() {
      this.subscribers.forEach((sub) => sub())
    }
  }

  ;(Object.keys(data) as Array<keyof IData>).forEach((key) => {
    let internalValue = data[key]
    const dep = new Dep()

    Object.defineProperty(data, key, {
      get() {
        dep.depend()
        return internalValue
      },
      set(newVal) {
        internalValue = newVal
        dep.notify()
      }
    })
  })

  function watcher(fn: () => void) {
    target = fn
    target()
    target = null
  }
  watcher(() => (total = data.price * data.quantity))
  watcher(() => (salePrice = data.price * 0.9))
})()
