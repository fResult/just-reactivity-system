# How to test

at the root directory, run `ts-node`
```bash
ts-node
```

then call `09-dep-class-n-watcher/index.ts`
```bash
.load 04-dep-class/index.ts
```

test: try to see `total` and `salePrice`
```typescript
console.log('total', total) // total 10
console.log('salePrice', salePrice) // salePrice 4.5
```

test: try to re-assign `data.price`
```bash
data.price = 20
```
then enter

test: log `total` and `salePrice` again
```typescript
console.log('total', total) // total 40
console.log('salePrice', salePrice) // salePrice 36
```
then enter, we will see they are updated immediately, coz they are reactived.
