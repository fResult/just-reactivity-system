# Reactivity system
This repo is my learning about how reactivity states works.

## How to test Reactivity state in this

1st: Install `typescript` and `ts-node`
```bash
yarn global add typescript ts-node
# or
npm install -g typescript ts-node
```

next:, run command follow child directories' README such as...
```bash
ts-node
# then...
.load 04-dep-class # to run script in `04-dep-class/index.ts` file
```
